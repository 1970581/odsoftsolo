package pt.isep.cms.students.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.students.client.StudentsService;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.students.shared.StudentDetails;

@SuppressWarnings("serial")
public class StudentsServiceImpl extends RemoteServiceServlet implements
StudentsService {

  private static final String[] studentsNameData = new String[] {
      "Hollie", "Emerson", "Healy", "Brigitte", "Elba", "Claudio",
      "Dena", "Christina", "Gail", "Orville", "Rae", "Mildred",
      "Candice", "Louise", "Emilio", "Geneva", "Heriberto", "Bulrush", 
      "Abigail", "Chad", "Terry", "Bell"};
  
  private final String[] studentsGenderData = new String[] {
		  
		  Student.MALE,Student.MALE,Student.MALE,Student.MALE,Student.MALE,Student.MALE,
		  Student.FEMALE,Student.FEMALE,Student.FEMALE,Student.FEMALE,Student.FEMALE,Student.FEMALE,
		  Student.MALE,Student.MALE,Student.MALE,Student.MALE,Student.MALE,Student.MALE,
		  Student.FEMALE,Student.FEMALE,Student.FEMALE,Student.FEMALE 
      };
  
  private final String[] studentsBirthdateData = new String[] {
      "01-09-2000", "02-09-2000", "13-09-2000",
      "11-07-2000", "11-03-2000", "14-09-2000",
      "11-05-2000", "17-09-2000", "16-09-2000",
      "11-04-2000", "14-09-2000", "15-09-2000",
      "11-04-2000", "11-09-2000", "15-09-2000",
      "11-06-2000", "13-09-2000", "17-09-2000",
      "11-08-2000", "21-09-2000", "14-09-2000",
      "11-09-2000"
      };
      
  private final HashMap<String, Student> students = new HashMap<String, Student>();

  public StudentsServiceImpl() {
    initStudents();
  }
  
  private void initStudents() {
    // TODO: Create a real UID for each student
    //
    for (int i = 0; i < studentsNameData.length && i < studentsGenderData.length && i < studentsBirthdateData.length; ++i) {
      Student student = new Student(String.valueOf(i), studentsNameData[i], studentsGenderData[i], studentsBirthdateData[i]);
      students.put(student.getId(), student); 
    }
  }
  
  public Student addStudent(Student student) {
    student.setId(String.valueOf(students.size()));
    students.put(student.getId(), student); 
    return student;
  }

  public Student updateStudent(Student student) {
	  String lid=student.getId();
    students.remove(student.getId());
    students.put(student.getId(), student); 
    return student;
  }

  public Boolean deleteStudent(String id) {
    students.remove(id);
    return true;
  }
  
  public ArrayList<StudentDetails> deleteStudents(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteStudent(ids.get(i));
    }
    
    return getStudentDetails();
  }
  
  public ArrayList<StudentDetails> getStudentDetails() {
    ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();
    
    Iterator<String> it = students.keySet().iterator();
    while(it.hasNext()) { 
      Student student = students.get(it.next());          
      studentDetails.add(student.getLightWeightStudent());
    }
    
    return studentDetails;
  }

  public Student getStudent(String id) {
    return students.get(id);
  }
}
