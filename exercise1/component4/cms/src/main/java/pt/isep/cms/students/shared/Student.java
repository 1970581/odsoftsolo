package pt.isep.cms.students.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Student implements Serializable {
	public String id;
  public String name;
  public String gender;
  public String birthdate;
  
  public static String MALE = "boy";
  public static String FEMALE = "girl";
	
	public Student() {}
	
	public Student(String id, String name, String gender, String birthdate) {
		this.id = id;
    this.name = name;
    this.birthdate = birthdate;
    if (gender.equals(MALE) || gender.equals(FEMALE)) this.gender = gender;
    else this.gender = MALE;
		
	}
	
	public StudentDetails getLightWeightStudent() {
	  return new StudentDetails(id, getFullInfo());
	}
	
  public String getId() { return id; }
  public void setId(String id) { this.id = id; }
  public String getName() { return name; }
  public void setName(String name) { this.name = name; }
  public String getGender() { return gender; }
  public void setGender(String gender) { this.gender = gender; }
  public String getBirthdate() { return birthdate; }
  public void setBirthdate(String birthdate) { this.birthdate = birthdate; }
  public String getFullInfo() { return name + ", sex: " + gender; }
}
