package pt.isep.cms.students;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;
import junit.framework.TestSuite;
import pt.isep.cms.contacts.client.ExampleGWTTest;

public class Example2GWTTestSuit extends GWTTestSuite {
	  public static Test suite() {
		    TestSuite suite = new TestSuite("Test for the Students Application");
		    suite.addTestSuite(ExampleGWTTest.class); 
		    return suite;
		  }
		} 